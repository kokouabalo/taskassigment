/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complexitee;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author abalo
 */
public class Couplage {

    private static Graph graphe;

    public Couplage() {
        graphe = new Graph();
    }

    public static Graph creeReseau(String fichier) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(fichier));
        String st;
        String[] t;
        int n = Integer.parseInt(br.readLine());
        System.out.println("  " + n);
        int perid=1;
        while ((st = br.readLine()) != null) {
            t = st.split(" ");
            Noeud pers = new Noeud(perid);
            pers.setName("p"+perid);
            for (int x = 0; x < t.length; x++) {
                Noeud task = new Noeud(Integer.parseInt(t[x]));
                task.setName("t"+Integer.parseInt(t[x]));
                int y=graphe.getIndiceGraph(task);
                if(y>-1){
                    pers.addChild(graphe.getNoeuds().get(y));
                }
                else{
                    graphe.addNoeud(task);
                    pers.addChild(task);
                }
               
                
            }
            graphe.addNoeud(pers);
            perid++;
            
            
        }
        return graphe;
    }
}
