package complexitee;

/**
 *
 * @author abalo
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class ShortestPath {

    // Graph node class
    public static class Node {

        int value;
        LinkedList<Node> children;

        // Basic constructor
        public Node(int value) {
            this.value = value;
        }

        // Lazily instantiate children list
        public void addChild(Node n) {
            if (this.children == null) {
                this.children = new LinkedList<>();
            }
            this.children.add(n);
        }

//        public Node[] getChildren(int parentValue) {
//            Node[] children = {;
//            if (this.children == null) {
//                this.children = new LinkedList<Node>();
//            }
//            this.children.add(n);
//        }
        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public LinkedList<Node> getChildren() {
            return children;
        }

        public void setChildren(LinkedList<Node> children) {
            this.children = children;
        }

    }

    // Find the shortest path between two nodes using BFS
    public static LinkedList<Node> shortestPath(Node a, Node b) {
        // Return null if either node is null or if they're the same node
        if (a == null || b == null) {
            return null;
        }
        if (a == b) {
            return null;
        }

        // Using a queue for our BFS
        Queue<Node> toVisit = new LinkedList<Node>();

        // Track the parents so that we can reconstruct our path
        HashMap<Node, Node> parents = new HashMap<Node, Node>();

        // Initialize the BFS
        toVisit.add(a);
        parents.put(a, null);

        // Keep going until we run out of nodes or reach our destination
        while (!toVisit.isEmpty()) {
            Node curr = toVisit.remove();

            // If we find the node we're looking for then we're done
            if (curr == b) {
                break;
            }

            // If the current node doesn't have children, skip it
            if (curr.children == null) {
                continue;
            }

            // Add all the children to the queue
            for (Node n : curr.children) {
                if (!parents.containsKey(n)) {
                    toVisit.add(n);
                    parents.put(n, curr);
                }
            }
        }

        // If we couldn't find a path, the destination node won't have been
        // added to our parents set
        if (parents.get(b) == null) {
            return null;
        }

        // Create the output list and add the path to the list
        LinkedList<Node> out = new LinkedList<Node>();
        Node n = b;
        while (n != null) {
            out.add(0, n);
            n = parents.get(n);
        }

        return out;
    }

    // Sample test cases
    public static Node[] creeReseau(String fichier) throws FileNotFoundException, IOException {
        Node[] graph = null;
        File file = new File(fichier);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        int count = 0;
        String[] t = {};
        int idPersonne = 3;
        while ((st = br.readLine()) != null) {
            if (count == 0) {
                int taille = Integer.valueOf(st) + 6;
                graph = new Node[taille];
                for (int i = 0; i < graph.length; i++) {
                    graph[i] = new Node(i + 1);
                }
            } else {
                //les id de 1 à 4 sont les salles 
                //de 5 à 8 sont les prof
                // 9 est le noeud s et 10 le noeud t
                t = st.split(" ");
                for (int i = 0; i < t.length; i++) {
                    graph[idPersonne].addChild(graph[Integer.valueOf(t[i]) - 1]);
                }

            }
            count++;
            idPersonne++;

        }
        //

        return graph;
    }

    public static void main(String[] args) throws IOException {

        Couplage c = new Couplage();
        Graph g = c.creeReseau("/home/abalo/NetBeansProjects/RO/src/complexitee/ro.txt");
        g.addSoursePuit();

        LinkedList<Noeud> l = g.cheminBFS(g.getNoeuds().getFirst(), g.getNoeuds().getLast());
        for (Noeud n : g.getNoeuds()) {
            if (n.getName().startsWith("p")) {
                System.out.println(" Personne " + n.getName());
                for (Noeud nn : n.getChildren()) {
                    System.out.print(" Taches " + nn.getName());
                }
                System.out.println("");
            }
        }
        for (Noeud nn : l) {
            System.out.print(" ->" + nn.getName());
        }
        System.out.println("");
        
        System.out.println("inverse  "+ g.inverseChemin(l));

//        //        /* Construct sample graph
//
//        Node[] graphe = creeReseau("C:\\Users\\NAT\\Desktop\\Mes cours\\RO\\ro.txt");
//        // ajout de s 
//        for (int i = 4; i < 8; i++) {
//            graphe[8].addChild(graphe[i]);
//        }
//        //ajout de t
//        for (int i = 0; i < 4; i++) {
//            graphe[i].addChild(graphe[9]);
//        }
//        for (Node n : graphe) {
//            System.out.print(" Noeud " + n.value + "Successeurs :");
//            if (n.children != null) {
//                for (Node nn : n.children) {
//                    System.out.print(" " + nn.value);
//                }
//
//            }
//            System.out.println("");
//        }
//        List<Node> l = shortestPath(graphe[8], graphe[9]);
//        for (Node n : l) {
//            System.out.print(" ->  " + n.value);
//        }
    }

    public boolean inverseChemin(LinkedList<Node> chemin) {

        LinkedList cheminInverse = new LinkedList();
        for (int i = chemin.size() - 1; i >= 0; i--) {
            cheminInverse.add(chemin.get(i));
        }
        return true;
    }

}
