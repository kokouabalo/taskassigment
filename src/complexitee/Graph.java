/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complexitee;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author abalo
 */
public class Graph {

    private LinkedList<Noeud> noeuds;

    public Graph() {
        this.noeuds = new LinkedList<>();
    }

    public void addNoeud(Noeud n) {
        this.noeuds.add(n);
    }

    public LinkedList<Noeud> getNoeuds() {
        return noeuds;
    }

    public void setNoeuds(LinkedList<Noeud> noeuds) {
        this.noeuds = noeuds;
    }

    public boolean inverseChemin(LinkedList<Noeud> chemin) {

        LinkedList<Noeud> cheminInverse = new LinkedList();
        for (int i = chemin.size() - 1; i >= 0; i--) {
            if (i > 0) {
                if (isChild(chemin.get(i), chemin.get(i - 1)) != true) {
                    return false;
                }
            }
            cheminInverse.add(chemin.get(i));
        }
        for (Noeud n : cheminInverse) {
            System.out.print(" -> " + n.getName());
        }
        System.out.println("");
        return true;
    }

    public boolean isChild(Noeud current, Noeud previous) {
        if (previous.getChildren().contains(current)) {
            return true;
        }
        return false;
    }

    public int getIndiceGraph(Noeud noeud) {
        int n = -1;
        if (!noeuds.contains(noeud)) {
            return -1;
        }
        for (int i = 0; i < noeuds.size(); i++) {
            if (noeuds.get(i).getId() == noeud.getId() && noeuds.get(i).getName().equals(noeud.getName())) {
                n = i;
                break;
            }
        }
        return n;
    }

    // Find the shortest path between two nodes using BFS
    public LinkedList<Noeud> cheminBFS(Noeud s, Noeud b) {

        // Return null if either node is null or if they're the same node
        if (s == null || b == null) {
            return null;
        }
        if (s == b) {
            return null;
        }

        // Using a queue for our BFS
        Queue<Noeud> toVisit = new LinkedList<Noeud>();

        // Track the parents so that we can reconstruct our path
        HashMap<Noeud, Noeud> parents = new HashMap<Noeud, Noeud>();

        // Initialize the BFS
        toVisit.add(s);
        parents.put(s, null);

        // Keep going until we run out of nodes or reach our destination
        while (!toVisit.isEmpty()) {
            Noeud curr = toVisit.remove();

            // If we find the node we're looking for then we're done
            if (curr == b) {
                break;
            }

            // If the current node doesn't have children, skip it
            if (curr.getChildren() == null) {
                continue;
            }

            // Add all the children to the queue
            for (Noeud n : curr.getChildren()) {
                if (!parents.containsKey(n)) {
                    toVisit.add(n);
                    parents.put(n, curr);
                }
            }
        }

        // If we couldn't find a path, the destination node won't have been
        // added to our parents set
        if (parents.get(b) == null) {
            return null;
        }

        // Create the output list and add the path to the list
        LinkedList<Noeud> out = new LinkedList<Noeud>();
        Noeud n = b;
        while (n != null) {
            out.add(0, n);
            n = parents.get(n);
        }

        return out;
    }

    public void addSoursePuit() {
        Noeud source = new Noeud(1);
        Noeud puit = new Noeud(1);
        source.setName("Source");
        puit.setName("Puit");
        for (Noeud n : noeuds) {
            if (n.getName().startsWith("t")) {
                n.addChild(puit);
            }

            if (n.getName().startsWith("p")) {
                source.addChild(n);
            }

        }
        noeuds.addFirst(source);
        noeuds.addLast(puit);

    }

    public int ff(Noeud s, Noeud t) {
        return 0;
    }
    
}
