/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complexitee;

import java.util.LinkedList;

/**
 *
 * @author abalo
 */
public class Noeud {

    private int id;
    private String name;
    private LinkedList<Noeud> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Noeud> getChildren() {
        return children;
    }

    public void setChildren(LinkedList<Noeud> children) {
        this.children = children;
    }

    
    public Noeud(int value) {
        this.id = value;
    }

    // Lazily instantiate children list
    public void addChild(Noeud n) {
        if (this.children == null) {
            this.children = new LinkedList<>();
        }
        this.children.add(n);
    }
}
